# PileUpServer

This rest api was developed within the module "Mobile Computing 2" in the winter semester 2017/2018 by Stefanie Schwarz, Tom�s K�nig and Tim Fischer.   
This api was connected to an android app, which was developed in parallel.

## Distribution of tasks

- Schwarz: Development (Android App), Unit Testing
- K�nig: Development (Android App), Unit Testing
- Fischer: Development (REST-API, Database, Android App)

## Prerequisite

- Install MongoDB (https://docs.mongodb.com/manual/installation/) 

## Install

1. Clone this repository
2. Download and integrate packages with maven (pom.xml)
3. Add run configuration (**IntelliJ IDEA**): 
    * Edit Configurations >> Add New Configuration > Spring Boot 
    * Rename the configuration for example to "Run"
    * Set main class to "Application"   

4. Start embedded tomcat webserver (start created "Run" task)
5. **Api-Doc** is available on url: http://127.0.0.1:8080/api/swagger-ui.html#/ (change port if necessary)

