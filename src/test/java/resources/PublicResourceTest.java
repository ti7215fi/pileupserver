package resources;

import control.AuthService;
import control.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import utils.exceptions.UserExistsException;
import utils.exceptions.UserUnknownException;
import view.LoginRequest;
import view.NewUser;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
public class PublicResourceTest {
    @Mock
    private AuthService authService;
    @Mock
    private UserService userService;
    @InjectMocks
    private PublicResource publicResource;

    @Test
    public void loginUnknownExceptionTest() {
        doThrow(UserUnknownException.class).when(authService).isUserValid(isA(LoginRequest.class));
        ResponseEntity responseEntity = publicResource.login(new LoginRequest());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void loginUnauthorizedTest() {
        when(authService.isUserValid(isA(LoginRequest.class))).thenReturn(false);
        ResponseEntity responseEntity = publicResource.login(new LoginRequest());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void registerUserUserExistsExceptionTest() {
        when(userService.registerUser(isA(NewUser.class))).thenThrow(UserExistsException.class);
        ResponseEntity responseEntity = publicResource.registerUser(new NewUser());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.CONFLICT);
    }

}
