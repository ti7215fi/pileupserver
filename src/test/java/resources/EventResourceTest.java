package resources;

import control.EventService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import utils.exceptions.CategoryUnknownException;
import utils.exceptions.EventUnknownException;
import utils.exceptions.UserUnknownException;
import view.EventCreate;
import view.InviteFriendRequest;
import view.JoinEventRequest;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
public class EventResourceTest {
    @Mock
    private EventService eventService;
    @InjectMocks
    private EventResource eventResource;

    @Test
    public void createEventUserUnknownExceptionTest() {
        when(eventService.createEvent(isA(EventCreate.class))).thenThrow(UserUnknownException.class);
        ResponseEntity responseEntity = eventResource.createEvent("Max", new EventCreate());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void createEventCategoryUnknownExceptionTest() {
        when(eventService.createEvent(isA(EventCreate.class))).thenThrow(CategoryUnknownException.class);
        ResponseEntity responseEntity = eventResource.createEvent("Max", new EventCreate());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void inviteFriendsToEventUserUnknownExceptionTest() {
        doThrow(UserUnknownException.class).when(eventService).inviteFriendsToEvent(isA(InviteFriendRequest.class), isA(String.class), isA(String.class));
        ResponseEntity responseEntity = eventResource.inviteFriendsToEvent("123", "Max", new InviteFriendRequest());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void inviteFriendsToEventEventUnknownExceptionTest() {
        doThrow(EventUnknownException.class).when(eventService).inviteFriendsToEvent(isA(InviteFriendRequest.class), isA(String.class), isA(String.class));
        ResponseEntity responseEntity = eventResource.inviteFriendsToEvent("123", "Max", new InviteFriendRequest());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void acceptEventRequestUserUnknownExceptionTest() {
        doThrow(UserUnknownException.class).when(eventService).acceptEventRequest(isA(String.class), isA(String.class),isA(Boolean.class));
        ResponseEntity responseEntity = eventResource.acceptEventRequest("Max", "123",true);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void acceptEventRequestEventUnknownExceptionTest() {
        doThrow(EventUnknownException.class).when(eventService).acceptEventRequest(isA(String.class), isA(String.class),isA(Boolean.class));
        ResponseEntity responseEntity = eventResource.acceptEventRequest("Max", "123",true);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void getEventsUserUnknownExceptionTest() {
        doThrow(UserUnknownException.class).when(eventService).getEventRequestsByUser(isA(String.class));
        ResponseEntity responseEntity = eventResource.getEvents("Max");
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void joinEventUserUnknownExceptionTest() {
        doThrow(UserUnknownException.class).when(eventService).joinEvent(isA(JoinEventRequest.class), isA(String.class));
        ResponseEntity responseEntity = eventResource.joinEvent("Max", new JoinEventRequest());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void joinEventEventUnknownExceptionTest() {
        doThrow(EventUnknownException.class).when(eventService).joinEvent(isA(JoinEventRequest.class), isA(String.class));
        ResponseEntity responseEntity = eventResource.joinEvent("Max", new JoinEventRequest());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }
}
