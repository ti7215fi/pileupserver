package resources;

import control.UserService;
import model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import utils.exceptions.UserUnknownException;
import view.GetCategoryResponse;
import view.SyncCategoriesRequest;

import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class CategoryResourceTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private CategoryResource categoryResource;

    @Test
    public void getCategoryByUserInvalidUsernameTest() {
        when(userService.getUserByUsername(isA(String.class))).thenThrow(UserUnknownException.class);
        ResponseEntity<GetCategoryResponse> responseEntity = categoryResource.getCategoriesByUser("test");
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void getCategoryByUserExistingUserTest() {
        when(userService.getUserByUsername(isA(String.class))).thenReturn(new User());
        ResponseEntity<GetCategoryResponse> responseEntity = categoryResource.getCategoriesByUser("test");
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void syncCategoriesNotExistingUsernameTest() {
        when(userService.getUserByUsername(isA(String.class))).thenThrow(UserUnknownException.class);
        ResponseEntity<GetCategoryResponse> responseEntity = categoryResource.getCategoriesByUser("test");
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void syncCategoriesExistingUsernameTest() {
        when(userService.getUserByUsername(isA(String.class))).thenReturn(new User());
        when(userService.updateUser(isA(User.class))).thenReturn(new User());
        ResponseEntity responseEntity = categoryResource.syncCategories("test", new SyncCategoriesRequest());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

}
