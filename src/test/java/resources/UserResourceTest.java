package resources;

import control.UserService;
import model.IUser;
import model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import utils.exceptions.UserUnknownException;
import view.GetUserResponse;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
public class UserResourceTest {
    @Mock
    private UserService userService;
    @InjectMocks
    private UserResource userResource;

    @Test
    public void getUserByUsernameUnknownExceptionTest() {
        when(userService.getUserByUsername(isA(String.class))).thenThrow(UserUnknownException.class);
        ResponseEntity<GetUserResponse> responseEntity = userResource.getUserByUsername(anyString());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void getUserByUsernameValidUsernameTest() {
        when(userService.getUserByUsername(isA(String.class))).thenReturn(new User());
        ResponseEntity<GetUserResponse> responseEntity = userResource.getUserByUsername(anyString());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void updateUserUserUnknownExceptionTest() {
        when(userService.getUserByUsername(isA(String.class))).thenThrow(UserUnknownException.class);
        ResponseEntity<GetUserResponse> responseEntity = userResource.getUserByUsername(anyString());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void updateUserUserValidUserTest() {
        when(userService.getUserByUsername(isA(String.class))).thenReturn(new User());
        when(userService.updateUser(isA(IUser.class))).thenReturn(new User());
        ResponseEntity<GetUserResponse> responseEntity = userResource.getUserByUsername(anyString());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void deleteUserUserValidUserTest()
    {
        when(userService.getUserByUsername(isA(String.class))).thenReturn(new User());
        ResponseEntity<GetUserResponse> responseEntity = userResource.deleteUser(anyString());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void deleteUserUserUserUnknownExceptionTest()
    {
        when(userService.getUserByUsername(isA(String.class))).thenThrow(UserUnknownException.class);
        ResponseEntity<GetUserResponse> responseEntity = userResource.deleteUser(anyString());
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }
}
