package control;

import model.IUser;
import model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import utils.exceptions.UserExistsException;
import utils.exceptions.UserUnknownException;
import view.CreateUserResponse;
import view.INewUser;
import view.NewUser;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class UserServiceTest {
    @InjectMocks
    private UserService userService;

    @Mock
    private IUserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Test(expected = UserExistsException.class)
    public void CreateResponseUserAlreadyExistsExceptionTest() {
        INewUser user = new NewUser();
        user.setUsername("max");
        user.setPassword("test");
        when(userRepository.exists("max")).thenReturn(true);
        Assert.assertEquals(userService.registerUser(user),null);
    }

    @Test
    public void CreateResponseWithNewUser()
    {
        CreateUserResponse expected = new CreateUserResponse();
        expected.setId("123");
        INewUser newUser = new NewUser();
        IUser user = new User();
        user.setId("123");

        when(userRepository.exists(isA(String.class))).thenReturn(false);
        when(passwordEncoder.encode(isA(String.class))).thenReturn("test");
        when(userRepository.insert(isA(IUser.class))).thenReturn(user);

        Assert.assertEquals(userService.registerUser(newUser).getId(),expected.getId());
    }

    @Test(expected = UserUnknownException.class)
    public void getUserByIdThrowUserUnknownExceptionTest()
    {
        when(userRepository.findByUsername(isA(String.class))).thenReturn(null);
        userService.getUserByUsername(isA(String.class));
    }

    @Test(expected = UserUnknownException.class)
    public void updateUserThrowUserUnknownException()
    {
        IUser user = new User();
        when(userRepository.exists(isA(String.class))).thenReturn(false);
        userService.updateUser(user);
    }

    @Test(expected = UserUnknownException.class)
    public void deleteUserThrowUserUnknownException()
    {
        IUser user = new User();
        when(userRepository.exists(isA(String.class))).thenReturn(false);
        userService.updateUser(user);
    }

}
