package control;

import model.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import utils.exceptions.CategoryUnknownException;
import utils.exceptions.UserUnknownException;
import view.CreateEventResponse;
import view.EventCompact;
import view.EventCreate;
import view.EventLocation;

import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
public class EventServiceTest {

    @Mock
    IUserRepository userRepository;

    @Mock
    IEventRepository eventRepository;

    @InjectMocks
    EventService eventService;

    @Test(expected = UserUnknownException.class)
    public void createEventThrowsExceptionIfUserWasNotFound() {
        when(userRepository.findByUsername("max")).thenReturn(null);

        EventCreate view = new EventCreate();
        view.setUsername("max");
        eventService.createEvent(view);
    }

    @PrepareForTest(CategoryValidator.class)
    @Test(expected = CategoryUnknownException.class)
    public void createEventThrowsExceptionIfCategoryIsInvalid() {
        IUser user = new User();
        user.setUsername("max");

        mockStatic(CategoryValidator.class);
        when(CategoryValidator.isValid("test")).thenReturn(false);

        when(userRepository.findByUsername("max")).thenReturn(user);

        EventCreate view = new EventCreate();
        view.setUsername("max");
        view.setCategory("test");
        eventService.createEvent(view);
    }

    @PrepareForTest(CategoryValidator.class)
    @Test
    public void createEventReturnsCreateEventResponseView() {
        IUser user = new User();
        user.setUsername("max");

        mockStatic(CategoryValidator.class);
        when(CategoryValidator.isValid("test")).thenReturn(true);

        when(userRepository.findByUsername("max")).thenReturn(user);

        EventLocation location = new EventLocation();
        location.setFormattedAddress("Teststraße 5a, 12345 Testhausen");
        location.setLatitude((double) 1234567);
        location.setLongitude((double) 7654321);

        EventCreate view = new EventCreate();
        view.setUsername("max");
        view.setCategory("test");
        view.setDate(new Date());
        view.setName("Neues Event");
        view.setEventLocation(location);

        IEvent event = new Event();
        view.assignToEvent(user, event);

        IEvent eventInserted = event;
        eventInserted.setId("eventInsertedId");

        when(eventRepository.insert(isA(IEvent.class))).thenReturn(eventInserted);

        CreateEventResponse result = eventService.createEvent(view);

        verify(userRepository, times(1)).save(isA(IUser.class));
        Assert.assertTrue(user.getCreatedEvents().contains(eventInserted));
        Assert.assertEquals(eventInserted.getId(), result.getId());
    }

    @Test(expected = UserUnknownException.class)
    public void getEventsByCreatorThrowsExceptionIfUserWasNotFound() {
        when(userRepository.findByUsername("max")).thenReturn(null);

        EventCreate view = new EventCreate();
        view.getUsername();
        eventService.getEventsByCreator("max");
    }

    @Test
    public void getEventsByCreatorReturnsView() {
        IUser user = new User();

        IEvent event1 = new Event();
        event1.setId("1");
        event1.setDate(new Date());
        event1.setLocation(new model.EventLocation());
        user.addCreatedEvent(event1);

        when(userRepository.findByUsername("max")).thenReturn(user);
        List<EventCompact> result = eventService.getEventsByCreator("max");

        Assert.assertEquals(1, result.size());
    }

    @Test(expected = UserUnknownException.class)
    public void getJoinedEventsByUserThrowsExceptionIfUserWasNotFound() {
        when(userRepository.findByUsername("max")).thenReturn(null);

        EventCreate view = new EventCreate();
        view.getUsername();
        eventService.getEventsByCreator("max");
    }

    @Test
    public void getJoinedEventsByUserReturnsView() {
        IUser user = new User();

        IEvent event2 = new Event();
        event2.setId("2");
        event2.setDate(new Date());
        event2.setLocation(new model.EventLocation());
        user.addCreatedEvent(event2);

        user.addJoinedEvent(event2);

        when(userRepository.findByUsername("max")).thenReturn(user);
        List<EventCompact> result = eventService.getJoinedEventsByUser("max");

        Assert.assertEquals(2, result.size());
    }

    @Test(expected = UserUnknownException.class)
    public void joinEventThrowsExceptionIfUserWasNotFound() {
        when(userRepository.findByUsername("max")).thenReturn(null);

        EventCreate view = new EventCreate();
        view.getUsername();
        eventService.getEventsByCreator("max");
    }
}
