package control;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import utils.exceptions.UserUnknownException;
import view.LoginRequest;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class AuthServiceTest {
    @Mock
    private IUserRepository userRepository;
    @InjectMocks
    private AuthService authService;

    @Test(expected = UserUnknownException.class)
    public void throwUserUnknownExceptionWhenUserNotFoundTest() {
        LoginRequest loginRequest = new LoginRequest();
        when(userRepository.findByUsername(isA(String.class))).thenReturn(null);
        authService.isUserValid(loginRequest);
    }
}
