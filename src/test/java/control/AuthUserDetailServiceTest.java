package control;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class AuthUserDetailServiceTest {

    @InjectMocks
    AuthUserDetailService authUserDetailService;

    @Mock
    private IUserRepository userRepository;

    @Test(expected = UsernameNotFoundException.class)
    public void throwUsernameNotFoundExceptionWhenUserNotFoundTest() {
        when(userRepository.findByUsername(isA(String.class))).thenReturn(null);
        authUserDetailService.loadUserByUsername("max");
    }
}
