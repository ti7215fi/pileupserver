package core;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * We don't use default login forms of spring security. So we need to override the default behavior of the
 * BasicAuthenticationEntryPoint class to prevent redirection to a login page after authentication failed.
 */
public class AuthBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    @Override
    public void commence(final HttpServletRequest request,
                         final HttpServletResponse response,
                         final AuthenticationException authException) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
     }

    @Override
    public void afterPropertiesSet() throws Exception {
        setRealmName("PILE_UP_REALM");
        super.afterPropertiesSet();
    }
}
