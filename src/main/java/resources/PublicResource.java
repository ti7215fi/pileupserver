package resources;

import control.AuthService;
import control.UserService;
import model.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import utils.exceptions.UserExistsException;
import utils.exceptions.UserUnknownException;
import view.CreateUserResponse;
import view.LoginRequest;
import view.NewUser;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/public")
public class PublicResource {

    private AuthService authService;
    private UserService userService;

    @Autowired
    public PublicResource(AuthService authService, UserService userService) {
        this.authService = authService;
        this.userService = userService;
    }

    @RequestMapping(
            value = "/login",
            method = RequestMethod.POST,
            consumes = "application/json"
    )
    public ResponseEntity login(@RequestBody LoginRequest view) {
        try {
            if(this.authService.isUserValid(view)) {
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (UserUnknownException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(
            value = "/register",
            method = RequestMethod.POST,
            consumes = "application/json"
    )
    public ResponseEntity registerUser(@RequestBody NewUser view) {
        try {
            CreateUserResponse response = this.userService.registerUser(view);
            return ResponseEntity
                    .created(new URI("/api/secure/user/" + response.getId()))
                    .body(response);
        } catch (UserExistsException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        } catch (URISyntaxException se) {
            se.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

}
