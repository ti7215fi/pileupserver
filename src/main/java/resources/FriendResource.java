package resources;

import control.FriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.exceptions.UserUnknownException;
import view.FriendList;
import view.FriendRequest;
import view.FriendRequestList;

@RestController
@RequestMapping("/secure/{username}/friend")
public class FriendResource {

    private FriendService friendService;

    @Autowired
    public FriendResource(FriendService friendService) {
        this.friendService = friendService;
    }

    @RequestMapping(
            value = "/{friendId}/react-request",
            method = RequestMethod.PATCH,
            consumes = "application/json"
    )
    public ResponseEntity reactOfFriendRequest(@PathVariable("username") String username,
                                             @PathVariable("friendId") String friendId,
                                             @RequestParam("accept") boolean accept) {
        try {
            this.friendService.reactOfFriendRequest(friendId, username, accept);
            return ResponseEntity.ok().build();
        } catch (UserUnknownException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(
            method = RequestMethod.GET,
            consumes = "application/json"
    )
    public @ResponseBody ResponseEntity<FriendList> getFriends(@PathVariable("username") String username) {
        try {
            FriendList body = this.friendService.getFriends(username);
            return ResponseEntity.ok().body(body);
        } catch (UserUnknownException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(
            value = "/requests",
            method = RequestMethod.GET,
            consumes = "application/json"
    )
    public @ResponseBody ResponseEntity<FriendRequestList> getFriendRequests(@PathVariable("username") String username) {
        try {
            FriendRequestList body = this.friendService.getFriendRequestsByUser(username);
            return ResponseEntity.ok().body(body);
        } catch (UserUnknownException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(
            value = "/send-request",
            method = RequestMethod.PATCH,
            consumes = "application/json"
    )
    public ResponseEntity sendFriendRequest(@PathVariable("username") String username, @RequestBody FriendRequest request) {
        try {
            this.friendService.sendFriendRequest(request, username);
            return ResponseEntity.ok().build();
        } catch (UserUnknownException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
