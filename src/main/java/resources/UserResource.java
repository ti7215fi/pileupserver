package resources;

import control.UserService;
import io.swagger.annotations.Api;
import model.IUser;
import model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.exceptions.UserExistsException;
import utils.exceptions.UserUnknownException;
import view.EditUser;
import view.GetUserResponse;
import view.MapUserList;
import view.NewUser;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/secure/user")
public class UserResource {

    private UserService userService;

    @Autowired
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(
            value = "{username}",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public @ResponseBody ResponseEntity<GetUserResponse> getUserByUsername(@PathVariable("username") String username) {
        try {
            IUser existingUser = this.userService.getUserByUsername(username);
            return ResponseEntity.ok().body(GetUserResponse.ofUser(existingUser));
        } catch (UserUnknownException uue) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(
            value = "{username}/map-users",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public @ResponseBody ResponseEntity<MapUserList> getUsersForMap(@PathVariable("username") String username) {
        try {
            MapUserList mapUserList = this.userService.getUsersForMap(username);
            return ResponseEntity.ok().body(mapUserList);
        } catch (UserUnknownException uue) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(
            value = "{username}",
            method = RequestMethod.PUT,
            consumes = "application/json",
            produces = "application/json"
    )
    public @ResponseBody ResponseEntity<EditUser> updateUser(@PathVariable("username") String username, @RequestBody EditUser view) {
        try {
            IUser existingUser = this.userService.getUserByUsername(username);
            view.assignToUser(existingUser);
            IUser updatedUser = this.userService.updateUser(existingUser);
            return ResponseEntity.ok().body(EditUser.ofUser(updatedUser));
        } catch (UserUnknownException uue) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(
            value = "{username}",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public ResponseEntity deleteUser(@PathVariable("username") String username)
    {
        try {
            IUser user = this.userService.getUserByUsername(username);
            this.userService.deleteUser(user.getId());
            return ResponseEntity.ok().build();
        } catch (UserUnknownException uue) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(
            value = "{username}/update-last-known-location",
            method = RequestMethod.PATCH,
            produces = "application/json"
    )
    public ResponseEntity updateLastKnownLocation(@PathVariable("username") String username, @RequestBody Location request)
    {
        try {
            this.userService.updateLastKnownLocation(username, request);
            return ResponseEntity.ok().build();
        } catch (UserUnknownException uue) {
            return ResponseEntity.notFound().build();
        }
    }

}
