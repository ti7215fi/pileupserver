package resources;

import control.UserService;
import model.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.exceptions.UserUnknownException;
import view.GetCategoryResponse;
import view.SyncCategoriesRequest;

import java.util.List;

@RestController
@RequestMapping("/secure/user/{username}/category")
public class CategoryResource {
    private UserService userService;

    @Autowired
    public CategoryResource(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(
            value = "",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public @ResponseBody ResponseEntity<GetCategoryResponse> getCategoriesByUser(@PathVariable("username") String username) {
        try {
            IUser user = this.userService.getUserByUsername(username);
            GetCategoryResponse response = new GetCategoryResponse();
            response.setCategoryTypes(user.getCategories());
            return ResponseEntity.ok().body(response);
        } catch (UserUnknownException uue) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(
            value = "/sync",
            method = RequestMethod.PUT,
            consumes = "application/json",
            produces = "application/json"
    )
    public ResponseEntity syncCategories(@PathVariable("username") String username, @RequestBody SyncCategoriesRequest view) {
        try {
            IUser user = this.userService.getUserByUsername(username);
            view.assignToCategories(user);
            this.userService.updateUser(user);
            return ResponseEntity.ok().build();
        } catch (UserUnknownException uue) {
            return ResponseEntity.notFound().build();
        }
    }
}
