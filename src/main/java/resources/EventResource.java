package resources;

import control.EventService;
import model.IEvent;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.exceptions.CategoryUnknownException;
import utils.exceptions.EventAlreadyJoinedException;
import utils.exceptions.EventUnknownException;
import utils.exceptions.UserUnknownException;
import view.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/secure/{username}/event")
public class EventResource {

    private EventService eventService;

    @Autowired
    public EventResource(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = "application/json"
    )
    public ResponseEntity createEvent(@PathVariable("username") String username, @RequestBody EventCreate view) {
        try {
            CreateEventResponse createdEvent = this.eventService.createEvent(view);
            return ResponseEntity
                    .created(new URI("/api/event/" + createdEvent.getId()))
                    .body(createdEvent);
        } catch (UserUnknownException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (CategoryUnknownException ce) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (URISyntaxException se) {
            se.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @RequestMapping(
            value = "/{eventId}/invite-friends",
            method = RequestMethod.PUT,
            consumes = "application/json"
    )
    public ResponseEntity inviteFriendsToEvent(@PathVariable("username") String username,
                                               @PathVariable("eventId") String eventId,
                                               @RequestBody InviteFriendRequest view) {
        try {
            this.eventService.inviteFriendsToEvent(view, eventId, username);
            return ResponseEntity.ok().build();
        } catch (UserUnknownException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (EventUnknownException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(
            value = "/{eventId}/react-request",
            method = RequestMethod.PATCH,
            consumes = "application/json"
    )
    public ResponseEntity acceptEventRequest(@PathVariable("username") String username,
                                             @PathVariable("eventId") String eventId,
                                             @RequestParam("accept") boolean accept) {
        try {
            this.eventService.acceptEventRequest(eventId, username, accept);
            return ResponseEntity.ok().build();
        } catch (UserUnknownException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (EventUnknownException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(
            method = RequestMethod.GET,
            consumes = "application/json"
    )
    public @ResponseBody ResponseEntity<GetEventRequests> getEvents(@PathVariable("username") String username) {
        try {
            GetEventRequests body = this.eventService.getEventRequestsByUser(username);
            return ResponseEntity.ok().body(body);
        } catch (UserUnknownException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(
            value = "/requests",
            method = RequestMethod.GET,
            consumes = "application/json"
    )
    public @ResponseBody ResponseEntity<GetEventResponse> getEventRequests(@PathVariable("username") String username) {
        try {
            GetEventResponse body = this.eventService.getEventsByUser(username);
            return ResponseEntity.ok().body(body);
        } catch (UserUnknownException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(
            value = "/join-event",
            method = RequestMethod.PATCH,
            consumes = "application/json"
    )
    public ResponseEntity joinEvent(@PathVariable("username") String username, @RequestBody JoinEventRequest request) {
        try {
            this.eventService.joinEvent(request, username);
            return ResponseEntity.ok().build();
        } catch (UserUnknownException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (EventUnknownException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
