package utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EventUnknownException extends IllegalStateException {
    public EventUnknownException(String eventId) {
        super("No event with id '" + eventId + "' found.");
    }
}
