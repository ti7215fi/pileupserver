package utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class EventAlreadyJoinedException extends IllegalStateException {
    public EventAlreadyJoinedException(String eventId) {
        super("Event with id '" + eventId + "' already joined.");
    }
}
