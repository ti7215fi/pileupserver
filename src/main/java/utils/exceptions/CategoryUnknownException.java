package utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CategoryUnknownException extends IllegalStateException {
    public CategoryUnknownException(String category) {
        super("No category with key '" + category + "' found.");
    }
}
