package utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class UserExistsException extends IllegalStateException {
    public UserExistsException(String userId) {
        super("User with id '" + userId + "' already exists.");
    }
}
