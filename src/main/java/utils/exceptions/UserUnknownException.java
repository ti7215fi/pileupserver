package utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UserUnknownException extends IllegalStateException{
    public UserUnknownException(String userId) {
        super("No user with id '" + userId + "' found.");
    }
}
