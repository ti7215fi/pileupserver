package view;

import com.fasterxml.jackson.annotation.JsonFormat;
import model.*;

import java.util.Date;

public class EventCreate {

    private String username;
    private String name;
    private String description;
    private String category;

    @JsonFormat(pattern = "dd.MM.yyyy HH:mm")
    private Date date;
    private EventLocation eventLocation;

    public void assignToEvent(IUser creator, IEvent event) {
        eventLocation.assignToEventLocation(event.getLocation());

        event.setName(name);
        event.setDescription(description);
        event.setDate(date);
        event.setCategory(category);
        event.setCreator(creator);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public EventLocation getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(EventLocation eventLocation) {
        this.eventLocation = eventLocation;
    }
}
