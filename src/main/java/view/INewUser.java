package view;

import model.IUser;

public interface INewUser {
    String getPassword();
    void setPassword(String password);

    String getFirstName();
    void setFirstName(String firstName);

    String getLastName();
    void setLastName(String lastName);

    String getUsername();
    void setUsername(String username);

    String getEmail();
    void setEmail(String email);

    void assignToUser(IUser user, String encodedPassword);
}
