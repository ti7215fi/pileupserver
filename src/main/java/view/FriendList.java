package view;

import model.IUser;

import java.util.ArrayList;
import java.util.List;

public class FriendList {

    private List<FriendCompact> friends;

    private FriendList() {
        this.friends = new ArrayList<>();
    }

    public static FriendList ofUser(List<IUser> friends) {
        FriendList view = new FriendList();
        for(IUser friend: friends) {
            view.friends.add(FriendCompact.ofUser(friend));
        }
        return view;
    }

    public List<FriendCompact> getFriends() {
        return friends;
    }

    public void setFriends(List<FriendCompact> friends) {
        this.friends = friends;
    }
}
