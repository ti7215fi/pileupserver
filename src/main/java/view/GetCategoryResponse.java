package view;

import java.util.List;

public class GetCategoryResponse {

    private List<String> categoryTypes;

    public List<String> getCategoryTypes() {
        return categoryTypes;
    }

    public void setCategoryTypes(List<String> categoryTypes) {
        this.categoryTypes = categoryTypes;
    }
}
