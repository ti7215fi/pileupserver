package view;

import model.IUser;

import java.util.ArrayList;
import java.util.List;

public class FriendRequestList {

    private List<FriendCompact> friendRequests;

    private FriendRequestList() {
        this.friendRequests = new ArrayList<>();
    }

    public static FriendRequestList ofUsers(List<IUser> users) {
        FriendRequestList requests = new FriendRequestList();
        for(IUser user: users) {
            requests.friendRequests.add(FriendCompact.ofUser(user));
        }
        return requests;
    }

    public List<FriendCompact> getFriendRequests() {
        return friendRequests;
    }

    public void setFriendRequests(List<FriendCompact> friendRequests) {
        this.friendRequests = friendRequests;
    }
}
