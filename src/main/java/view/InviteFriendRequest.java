package view;

import java.util.List;

public class InviteFriendRequest {

    private List<String> friendIds;

    public List<String> getFriendIds() {
        return friendIds;
    }

    public void setFriendIds(List<String> friendIds) {
        this.friendIds = friendIds;
    }
}
