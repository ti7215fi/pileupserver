package view;

import model.IUser;

import java.util.ArrayList;
import java.util.List;

public class MapUserList {

    private List<MapUser> users;

    private MapUserList() {
        this.users = new ArrayList<>();
    }

    public static MapUserList ofUsers (List<IUser> users) {
        MapUserList mapUserList = new MapUserList();
        for(IUser user: users) {
            mapUserList.users.add(MapUser.ofUser(user));
        }
        return mapUserList;
    }

    public List<MapUser> getUsers() {
        return users;
    }

    public void setUsers(List<MapUser> users) {
        this.users = users;
    }
}
