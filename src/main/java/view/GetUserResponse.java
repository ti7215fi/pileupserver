package view;

import model.IUser;

public class GetUserResponse {

    private String id;
    private String username;
    private String password = "toremove";
    private String firstName;
    private String lastName;
    private String city;
    private String email;

    public static GetUserResponse ofUser(IUser user) {
        GetUserResponse response = new GetUserResponse();
        response.id = user.getId();
        response.username = user.getUsername();
        response.firstName = user.getFirstName();
        response.lastName = user.getLastName();
        response.city = user.getAddress().getCity();
        response.email = user.getEmail();
        return response;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
