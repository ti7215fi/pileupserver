package view;

import model.IEvent;

public class CreateEventResponse {

    private String id;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static CreateEventResponse ofEvent(IEvent event) {
        CreateEventResponse view = new CreateEventResponse();
        view.setId(event.getId());
        return view;
    }

}
