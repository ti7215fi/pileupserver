package view;

import model.IUser;

public interface ISyncCategoriesRequest {

    void assignToCategories(IUser user);
}
