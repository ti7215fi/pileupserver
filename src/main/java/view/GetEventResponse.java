package view;

import model.IEvent;

import java.util.ArrayList;
import java.util.List;

public class GetEventResponse {

    private List<EventCompact> pastEvents;
    private List<EventCompact> futureEvents;

    private GetEventResponse() {
        this.pastEvents = new ArrayList<>();
        this.futureEvents = new ArrayList<>();
    }

    public static GetEventResponse ofEvents(List<IEvent> pastEvents, List<IEvent> futureEvents) {
        GetEventResponse response = new GetEventResponse();

        for(IEvent pastEvent: pastEvents) {
            response.pastEvents.add(EventCompact.ofEvent(pastEvent));
        }

        for(IEvent futureEvent: futureEvents) {
            response.futureEvents.add(EventCompact.ofEvent(futureEvent));
        }

        return response;
    }

    public List<EventCompact> getPastEvents() {
        return pastEvents;
    }

    public void setPastEvents(List<EventCompact> pastEvents) {
        this.pastEvents = pastEvents;
    }

    public List<EventCompact> getFutureEvents() {
        return futureEvents;
    }

    public void setFutureEvents(List<EventCompact> futureEvents) {
        this.futureEvents = futureEvents;
    }
}
