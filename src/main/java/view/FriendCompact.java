package view;

import model.IUser;

public class FriendCompact {

    private String id;
    private String username;
    private String firstName;
    private String lastName;

    public static FriendCompact ofUser(IUser user) {
        FriendCompact view = new FriendCompact();
        view.id = user.getId();
        view.username = user.getUsername();
        view.firstName = user.getFirstName();
        view.lastName = user.getLastName();
        return view;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
