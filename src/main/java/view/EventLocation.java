package view;

import model.IEventLocation;

public class EventLocation {
    private Double longitude;
    private Double latitude;
    private String formattedAddress;

    public void assignToEventLocation(IEventLocation location) {
        location.setLongitude(longitude);
        location.setLatitude(latitude);
        location.setFormattedAddress(formattedAddress);
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }
}
