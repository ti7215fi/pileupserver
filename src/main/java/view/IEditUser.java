package view;

import model.IUser;

public interface IEditUser {
    String getFirstName();
    String getLastName();
    String getCity();

    void assignToUser(IUser user);
}
