package view;

import model.IEvent;

import java.util.ArrayList;
import java.util.List;

public class GetEventRequests {

    List<EventCompact> eventRequests;

    private GetEventRequests() {
        this.eventRequests = new ArrayList<>();
    }

    public static GetEventRequests ofEvents(List<IEvent> events) {
        GetEventRequests requests = new GetEventRequests();
        for(IEvent event: events) {
            requests.eventRequests.add(EventCompact.ofEvent(event));
        }
        return requests;
    }

    public List<EventCompact> getEventRequests() {
        return eventRequests;
    }

    public void setEventRequests(List<EventCompact> eventRequests) {
        this.eventRequests = eventRequests;
    }
}
