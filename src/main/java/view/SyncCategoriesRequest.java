package view;

import model.IUser;

import java.util.ArrayList;

public class SyncCategoriesRequest implements ISyncCategoriesRequest {

    private ArrayList<String> categoriesToSync;

    @Override
    public void assignToCategories(IUser user) {
        user.clearCategories();
        user.setCategories(categoriesToSync);
    }

    public ArrayList<String> getCategoriesToSync() {
        return categoriesToSync;
    }

    public void setCategoriesToSync(ArrayList<String> categoriesToSync) {
        this.categoriesToSync = categoriesToSync;
    }
}
