package view;

import model.IUser;

public class EditUser implements IEditUser{
    private String firstName;
    private String lastName;
    private String city;

    @Override
    public String getFirstName() {
        return this.firstName;
    }


    @Override
    public String getLastName() {
        return this.lastName;
    }


    @Override
    public String getCity() {
        return this.city;
    }

    @Override
    public void assignToUser(IUser user) {
        user.setLastName(this.lastName);
        user.setFirstName(this.firstName);
        user.getAddress().setCity(this.city);
    }

    public static EditUser ofUser(IUser user) {
        EditUser view = new EditUser();
        view.firstName = user.getFirstName();
        view.lastName = user.getLastName();
        view.city = user.getAddress().getCity();
        return view;
    }
}
