package view;

public class FriendRequest {

    private String userToInviteId;

    public String getUserToInviteId() {
        return userToInviteId;
    }

    public void setUserToInviteId(String userToInviteId) {
        this.userToInviteId = userToInviteId;
    }
}
