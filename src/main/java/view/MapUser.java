package view;

import model.IUser;
import model.Location;

public class MapUser {

    private String username;
    private String id;
    private Location location;

    public static MapUser ofUser(IUser user) {
        MapUser u = new MapUser();
        u.id = user.getId();
        u.location = user.getLastKnownLocation();
        u.username = user.getUsername();
        return u;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
