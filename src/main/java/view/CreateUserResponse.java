package view;

import model.IUser;

public class CreateUserResponse {

    private String id;

    public static CreateUserResponse ofUser(IUser user) {
        CreateUserResponse view = new CreateUserResponse();
        view.setId(user.getId());
        return view;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
