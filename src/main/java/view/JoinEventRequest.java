package view;

public class JoinEventRequest {

    private String eventToJoin;

    public String getEventToJoin() {
        return eventToJoin;
    }

    public void setEventToJoin(String eventToJoin) {
        this.eventToJoin = eventToJoin;
    }

}
