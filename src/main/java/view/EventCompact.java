package view;

import model.IEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class EventCompact {

    private String id;
    private String title;
    private String dateTime;
    private String place;

    public static EventCompact ofEvent(IEvent event) {
        DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance();

        EventCompact view = new EventCompact();
        view.id = event.getId();
        view.title = event.getName();
        view.dateTime = dateFormat.format(event.getDate());
        view.place = event.getLocation().getFormattedAddress();
        return view;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
