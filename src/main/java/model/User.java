package model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.List;

public class User implements IUser{

    @Id
    private String id;

    @Indexed(unique = true)
    private String username;

    @Indexed(unique = true)
    private String email;

    private String password;
    private String firstName;
    private String lastName;
    private Address address;
    private Location lastKnownLocation;
    private List<String> categories;

    @DBRef
    private List<IEvent> createdEvents;

    @DBRef
    private List<IEvent> joinedEvents;

    @DBRef
    private List<IEvent> requestForEvents;

    private List<String> friends;
    private List<String> friendRequests;

    public User() {
        this.address = new Address();
        this.categories = new ArrayList<>();
        this.createdEvents = new ArrayList<>();
        this.joinedEvents = new ArrayList<>();
        this.requestForEvents = new ArrayList<>();
        this.friends = new ArrayList<>();
        this.friendRequests = new ArrayList<>();
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return this.lastName;
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Location getLastKnownLocation() {
        return this.lastKnownLocation;
    }

    @Override
    public void setLastKnownLocation(Location location) {
        this.lastKnownLocation = location;
    }

    @Override
    public List<String> getCategories() {
        return this.categories;
    }

    @Override
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @Override
    public void addCategory(String category) {
        this.categories.add(category);
    }

    @Override
    public void removeCategory(String category) {
        this.categories.remove(category);
    }

    @Override
    public void clearCategories() {
        this.categories.clear();
    }

    @Override
    public List<IEvent> getCreatedEvents() {
        return this.createdEvents;
    }

    @Override
    public void addCreatedEvent(IEvent event) {
        this.createdEvents.add(event);
        this.joinedEvents.add(event);
    }

    @Override
    public void removeCreatedEvent(IEvent event) {
        this.createdEvents.remove(event);
    }

    @Override
    public List<IEvent> getJoinedEvents() {
        return joinedEvents;
    }

    @Override
    public void addJoinedEvent(IEvent event) {
        this.joinedEvents.add(event);
    }

    @Override
    public void removeJoinedEvent(IEvent event) {
        this.joinedEvents.remove(event);
    }

    @Override
    public List<IEvent> getRequestsForEvents() {
        return this.requestForEvents;
    }

    @Override
    public void addRequestForEvent(IEvent event) {
        this.requestForEvents.add(event);
    }

    @Override
    public void removeRequestForEvent(IEvent event) {
        this.requestForEvents.remove(event);
    }

    @Override
    public List<String> getFriendRequests() {
        return this.friendRequests;
    }

    @Override
    public void addFriendRequest(String username) {
        this.friendRequests.add(username);
    }

    @Override
    public void removeFriendRequest(String username) {
        this.friendRequests.remove(username);
    }

    @Override
    public List<String> getFriends() {
        return friends;
    }

    @Override
    public void addFriend(String username) {
        this.friends.add(username);
    }

    @Override
    public void removeFriend(String username) {
        this.friends.remove(username);
    }
}
