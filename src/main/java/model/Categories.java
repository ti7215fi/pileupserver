package model;

public final class Categories {
    public static final String BBQ = "BBQ";
    public static final String BOATING = "BOATING";
    public static final String BOWLING = "BOWLING";
    public static final String CAMPING = "CAMPING";
    public static final String CLIMBING = "CLIMBING";
    public static final String COOKING = "COOKING";
    public static final String CYCLE = "CYCLE";
    public static final String FISHING = "FISHING";
    public static final String FITNESS = "FITNESS";
    public static final String HIKEING = "HIKEING";
    public static final String RIDING = "RIDING";
    public static final String MUSHROOM = "MUSHROOM";
    public static final String SWIMMING = "SWIMMING";

    public static String[] getAll() {
        return new String[]{
                BBQ, BOATING, BOWLING, CAMPING, CLIMBING,
                COOKING, CYCLE, FISHING, FITNESS, HIKEING,
                RIDING, MUSHROOM, SWIMMING
        };
    }
}
