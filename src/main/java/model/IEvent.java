package model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "events")
public interface IEvent {

    String getId();
    void setId(String id);

    String getName();
    void setName(String name);

    Date getDate();
    void setDate(Date date);

    String getDescription();
    void setDescription(String description);

    boolean isClosed();

    String getCategory();
    void setCategory(String category);

    IUser getCreator();
    void setCreator(IUser creator);

    IEventLocation getLocation();
    void setLocation(IEventLocation location);
}
