package model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "users")
public interface IUser {

    String getId();
    void setId(String id);

    String getPassword();
    void setPassword(String password);

    String getFirstName();
    void setFirstName(String firstName);

    String getLastName();
    void setLastName(String lastName);

    Address getAddress();
    void setAddress(Address address);

    String getUsername();
    void setUsername(String username);

    String getEmail();
    void setEmail(String email);

    Location getLastKnownLocation();
    void setLastKnownLocation(Location location);

    List<String> getCategories();
    void setCategories(List<String> categories);
    void addCategory(String category);
    void removeCategory(String category);
    void clearCategories();

    List<IEvent> getCreatedEvents();
    void addCreatedEvent(IEvent event);
    void removeCreatedEvent(IEvent event);

    List<IEvent> getJoinedEvents();
    void addJoinedEvent(IEvent event);
    void removeJoinedEvent(IEvent event);

    List<IEvent> getRequestsForEvents();
    void addRequestForEvent(IEvent event);
    void removeRequestForEvent(IEvent event);

    List<String> getFriendRequests();
    void addFriendRequest(String username);
    void removeFriendRequest(String username);

    List<String> getFriends();
    void addFriend(String username);
    void removeFriend(String username);

}
