package model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Event implements IEvent {

    @Id
    private String id;
    private String name;
    private Date date;
    private String description;
    private String category;

    @DBRef
    private User creator;
    private EventLocation location;

    public Event() {
        this.location = new EventLocation();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean isClosed() {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        return this.date.before(today);
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public IUser getCreator() {
        return creator;
    }

    @Override
    public void setCreator(IUser creator) {
        this.creator = (User) creator;
    }

    @Override
    public IEventLocation getLocation() {
        return location;
    }

    @Override
    public void setLocation(IEventLocation location) {
        this.location = (EventLocation) location;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (!(obj instanceof Event)) { return false; }
        if (obj == this) { return true; }

        Event event = (Event)obj;

        if (event.getId() == null || this.getId() == null) { return false; }

        return event.getId().equals(this.getId());
    }

}
