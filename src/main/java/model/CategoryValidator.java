package model;

import java.util.Arrays;

public final class CategoryValidator {
    public static boolean isValid(String categoryName) {
        return Arrays.asList(Categories.getAll()).indexOf(categoryName) != -1;
    }
}
