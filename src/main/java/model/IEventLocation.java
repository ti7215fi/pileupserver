package model;

public interface IEventLocation {

    Double getLongitude();
    void setLongitude(Double longitude);

    Double getLatitude();
    void setLatitude(Double latitude);

    String getFormattedAddress();
    void setFormattedAddress(String formattedAddress);

}
