package control;

import model.IEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEventRepository extends MongoRepository<IEvent, String> {
}
