package control;

import view.LoginRequest;

public interface IAuthService {
    boolean isUserValid(LoginRequest view);
}
