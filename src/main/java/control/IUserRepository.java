package control;

import model.IUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends MongoRepository<IUser, String> {
    @Query(value = "{'username': ?0}")
    IUser findByUsername(String username);
    @Query(value = "{'username': ?0, 'password': ?1}")
    IUser findByUsernameAndPassword(String username, String password);
}
