package control;

import view.*;

import java.util.List;

public interface IEventService {

    CreateEventResponse createEvent(EventCreate view);
    GetEventResponse getEventsByUser(String username);
    List<EventCompact> getEventsByCreator(String username);
    List<EventCompact> getJoinedEventsByUser(String username);
    void joinEvent(JoinEventRequest request, String username);
    void inviteFriendsToEvent(InviteFriendRequest request, String eventId, String username);
    void acceptEventRequest(String eventId, String username, boolean accept);
    GetEventRequests getEventRequestsByUser(String username);


}
