package control;

import model.IUser;
import model.Location;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import utils.exceptions.UserExistsException;
import utils.exceptions.UserUnknownException;
import view.CreateUserResponse;
import view.GetUserResponse;
import view.INewUser;
import view.MapUserList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService implements IUserService {

    private IUserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(IUserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public CreateUserResponse registerUser(INewUser view) throws UserExistsException {
        if(this.userRepository.exists(view.getUsername())) {
            throw new UserExistsException(view.getUsername());
        }
        String encodedPassword = passwordEncoder.encode(view.getPassword());

        User newUser = new User();
        view.assignToUser(newUser, encodedPassword);
        newUser = this.userRepository.insert(newUser);
        return CreateUserResponse.ofUser(newUser);
    }

    @Override
    public IUser getUserByUsername(String username) throws UserUnknownException {
        IUser existingUser = this.userRepository.findByUsername(username);
        if(existingUser == null) {
            throw new UserUnknownException(username);
        }
        return existingUser;
    }

    @Override
    public IUser updateUser(IUser newUserData) throws UserUnknownException {
        if(!this.userRepository.exists(newUserData.getId())) {
            throw new UserUnknownException(newUserData.getId());
        }
        return this.userRepository.save(newUserData);
    }

    @Override
    public void deleteUser(String userId) throws UserUnknownException {
        if(!this.userRepository.exists(userId)) {
            throw new UserUnknownException(userId);
        }
        this.userRepository.delete(userId);
    }

    @Override
    public void updateLastKnownLocation(String username, Location location) {
        IUser existingUser = this.userRepository.findByUsername(username);
        if(existingUser == null) {
            throw new UserUnknownException(username);
        }
        existingUser.setLastKnownLocation(location);
        this.userRepository.save(existingUser);
    }

    @Override
    public MapUserList getUsersForMap(String username) {
        IUser existingUser = this.userRepository.findByUsername(username);
        if(existingUser == null) {
            throw new UserUnknownException(username);
        }
        List<IUser> allUsers = this.userRepository.findAll();
        List<IUser> users = new ArrayList<>();

        for(IUser user: allUsers) {
            if (user.getLastKnownLocation() != null && !user.getId().equals(existingUser.getId())) {
                users.add(user);
            }
        }

        return MapUserList.ofUsers(users);
    }
}
