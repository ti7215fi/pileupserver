package control;

import model.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utils.exceptions.UserUnknownException;
import view.FriendList;
import view.FriendRequest;
import view.FriendRequestList;

import java.util.ArrayList;
import java.util.List;

@Service
public class FriendService implements IFriendService {

    private IUserRepository userRepository;

    @Autowired
    public FriendService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public FriendList getFriends(String username) {
        IUser user = this.userRepository.findByUsername(username);
        if(user == null) {
            throw new UserUnknownException(username);
        }
        List<IUser> friends = new ArrayList<>();
        for(String name: user.getFriends()) {
            IUser friend = this.userRepository.findByUsername(name);
            if (friend != null) {
                friends.add(friend);
            }
        }
        return FriendList.ofUser(friends);
    }

    @Override
    public void sendFriendRequest(FriendRequest request, String username) {
        IUser user = this.userRepository.findByUsername(username);
        if(user == null) {
            throw new UserUnknownException(username);
        }
        IUser userToInvite = this.userRepository.findOne(request.getUserToInviteId());
        if(userToInvite == null) {
            throw new UserUnknownException(username);
        }
        userToInvite.addFriendRequest(user.getUsername());
        this.userRepository.save(userToInvite);
    }

    @Override
    public FriendRequestList getFriendRequestsByUser(String username) {
        IUser user = this.userRepository.findByUsername(username);
        if(user == null) {
            throw new UserUnknownException(username);
        }
        List<IUser> users = new ArrayList<>();
        for(String name: user.getFriendRequests()) {
            IUser friend = this.userRepository.findByUsername(name);
            if(friend != null) {
                users.add(friend);
            }
        }

        return FriendRequestList.ofUsers(users);
    }

    @Override
    public void reactOfFriendRequest(String friendId, String username, boolean accept) {
        IUser user = this.userRepository.findByUsername(username);
        if(user == null) {
            throw new UserUnknownException(username);
        }
        IUser friend = this.userRepository.findOne(friendId);
        if(friend == null) {
            throw new UserUnknownException(username);
        }
        friend.removeFriendRequest(user.getUsername());

        if(accept) {
            user.addFriend(friend.getUsername());
            friend.addFriend(user.getUsername());
        }

        this.userRepository.save(user);
        this.userRepository.save(friend);
    }
}
