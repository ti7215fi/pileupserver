package control;

import view.FriendList;
import view.FriendRequest;
import view.FriendRequestList;

public interface IFriendService {

    FriendList getFriends(String username);
    void sendFriendRequest(FriendRequest userToInviteId, String username);
    FriendRequestList getFriendRequestsByUser(String username);
    void reactOfFriendRequest(String friendId, String username, boolean accept);

}
