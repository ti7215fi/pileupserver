package control;

import model.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import utils.exceptions.UserUnknownException;
import view.LoginRequest;

@Service
public class AuthService implements IAuthService {

    private IUserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AuthService(IUserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public boolean isUserValid(LoginRequest view) throws UserUnknownException {
        IUser user = this.userRepository.findByUsername(view.getUsername());
        if(user == null) {
            throw new UserUnknownException(view.getUsername());
        }
        return this.passwordEncoder.matches(view.getPassword(), user.getPassword());
    }

}
