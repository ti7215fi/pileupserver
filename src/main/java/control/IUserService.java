package control;

import model.IUser;
import model.Location;
import view.CreateUserResponse;
import view.GetUserResponse;
import view.INewUser;
import view.MapUserList;

public interface IUserService {

    CreateUserResponse registerUser(INewUser view);

    IUser getUserByUsername(String userName);

    IUser updateUser(IUser newUserData);

    void deleteUser(String userId);

    void updateLastKnownLocation(String username, Location location);

    MapUserList getUsersForMap(String username);

}
