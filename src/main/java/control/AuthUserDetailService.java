package control;

import model.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthUserDetailService implements UserDetailsService {

    private IUserRepository userRepository;

    @Autowired
    public AuthUserDetailService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        IUser user = this.userRepository.findByUsername(username);

        if(user == null)
        {
            throw new UsernameNotFoundException(username);
        }

        return new AuthUserDetails(user);
    }
}
