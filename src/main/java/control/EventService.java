package control;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utils.exceptions.CategoryUnknownException;
import utils.exceptions.EventAlreadyJoinedException;
import utils.exceptions.EventUnknownException;
import utils.exceptions.UserUnknownException;
import view.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventService implements IEventService {

    private IUserRepository userRepository;
    private IEventRepository eventRepository;

    @Autowired
    public EventService(IUserRepository userRepository, IEventRepository eventRepository) {
        this.userRepository = userRepository;
        this.eventRepository = eventRepository;
    }

    @Override
    public CreateEventResponse createEvent(EventCreate view) throws UserUnknownException, CategoryUnknownException {
        Event newEvent = new Event();
        IUser creator = this.userRepository.findByUsername(view.getUsername());
        if(creator == null) {
            throw new UserUnknownException(view.getUsername());
        }
        boolean validCategory = CategoryValidator.isValid(view.getCategory());
        if (!validCategory) {
            throw new CategoryUnknownException(view.getCategory());
        }
        view.assignToEvent(creator, newEvent);
        newEvent = this.eventRepository.insert(newEvent);
        creator.addCreatedEvent(newEvent);
        this.userRepository.save(creator);
        return CreateEventResponse.ofEvent(newEvent);
    }

    @Override
    public GetEventResponse getEventsByUser(String username) {
        IUser user = this.userRepository.findByUsername(username);
        if(user == null) {
            throw new UserUnknownException(username);
        }

        List<IEvent> allEvents = this.eventRepository.findAll();
        List<IEvent> pastEvents = allEvents
                .stream()
                .filter(IEvent::isClosed)
                .filter(e -> user.getJoinedEvents().contains(e))
                .collect(Collectors.toList());

        List<IEvent> futureEvents = allEvents
                .stream()
                .filter(e -> !e.isClosed())
                .filter(e -> user.getCategories().contains(e.getCategory()))
                .collect(Collectors.toList());

        return GetEventResponse.ofEvents(pastEvents, futureEvents);
    }

    @Override
    public List<EventCompact> getEventsByCreator(String username) throws UserUnknownException {
        IUser creator = this.userRepository.findByUsername(username);
        if(creator == null) {
            throw new UserUnknownException(username);
        }
        List<IEvent> events = creator.getCreatedEvents();
        List<EventCompact> view = new ArrayList<>();
        for (IEvent event : events) {
            view.add(EventCompact.ofEvent(event));
        }
        return view;
    }

    @Override
    public List<EventCompact> getJoinedEventsByUser(String username) {
        IUser user = this.userRepository.findByUsername(username);
        if(user == null) {
            throw new UserUnknownException(username);
        }
        List<IEvent> events = user.getJoinedEvents();
        List<EventCompact> view = new ArrayList<>();
        for (IEvent event : events) {
            view.add(EventCompact.ofEvent(event));
        }
        return view;
    }

    @Override
    public void joinEvent(JoinEventRequest request, String username) throws UserUnknownException, EventUnknownException, EventAlreadyJoinedException {
        IUser user = this.userRepository.findByUsername(username);
        if(user == null) {
            throw new UserUnknownException(username);
        }
        IEvent event = this.eventRepository.findOne(request.getEventToJoin());
        if(event == null) {
            throw new EventUnknownException(request.getEventToJoin());
        }
        List<IEvent> joinedEvents = user.getJoinedEvents();
        if(joinedEvents.contains(event)) {
            throw new EventAlreadyJoinedException(event.getId());
        }

        user.addJoinedEvent(event);
        this.userRepository.save(user);
    }

    @Override
    public void inviteFriendsToEvent(InviteFriendRequest request, String eventId, String username) {
        IUser userWhoInvites = this.userRepository.findByUsername(username);
        if(userWhoInvites == null) {
            throw new UserUnknownException(username);
        }
        IEvent event = this.eventRepository.findOne(eventId);
        if(event == null) {
            throw new EventUnknownException(eventId);
        }

        List<IUser> invitedFriends = new ArrayList<>();
        for(String friendId: request.getFriendIds()) {
            IUser friend = this.userRepository.findOne(friendId);
            if(friend != null) {
                friend.addRequestForEvent(event);
                invitedFriends.add(friend);
            } else {
                // ToDo
            }
        }
        this.userRepository.save(invitedFriends);
    }

    @Override
    public void acceptEventRequest(String eventId, String username, boolean accept) {
        IUser user = this.userRepository.findByUsername(username);
        if(user == null) {
            throw new UserUnknownException(username);
        }
        IEvent event = this.eventRepository.findOne(eventId);
        if(event == null) {
            throw new EventUnknownException(eventId);
        }
        user.removeRequestForEvent(event);

        if(accept) {
            user.addJoinedEvent(event);
        }

        this.userRepository.save(user);
    }

    @Override
    public GetEventRequests getEventRequestsByUser(String username) {
        IUser user = this.userRepository.findByUsername(username);
        if(user == null) {
            throw new UserUnknownException(username);
        }
        return GetEventRequests.ofEvents(user.getRequestsForEvents());
    }


}
